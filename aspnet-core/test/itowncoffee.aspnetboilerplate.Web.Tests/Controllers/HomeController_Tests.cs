﻿using System.Threading.Tasks;
using itowncoffee.aspnetboilerplate.Models.TokenAuth;
using itowncoffee.aspnetboilerplate.Web.Controllers;
using Shouldly;
using Xunit;

namespace itowncoffee.aspnetboilerplate.Web.Tests.Controllers
{
    public class HomeController_Tests: aspnetboilerplateWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}