﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using itowncoffee.aspnetboilerplate.EntityFrameworkCore;
using itowncoffee.aspnetboilerplate.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace itowncoffee.aspnetboilerplate.Web.Tests
{
    [DependsOn(
        typeof(aspnetboilerplateWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class aspnetboilerplateWebTestModule : AbpModule
    {
        public aspnetboilerplateWebTestModule(aspnetboilerplateEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(aspnetboilerplateWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(aspnetboilerplateWebMvcModule).Assembly);
        }
    }
}