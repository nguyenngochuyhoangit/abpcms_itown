using System.ComponentModel.DataAnnotations;

namespace itowncoffee.aspnetboilerplate.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}