﻿using System.Threading.Tasks;
using Abp.Application.Services;
using itowncoffee.aspnetboilerplate.Authorization.Accounts.Dto;

namespace itowncoffee.aspnetboilerplate.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
