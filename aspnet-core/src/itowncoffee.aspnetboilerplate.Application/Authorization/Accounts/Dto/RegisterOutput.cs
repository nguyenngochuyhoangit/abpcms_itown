﻿namespace itowncoffee.aspnetboilerplate.Authorization.Accounts.Dto
{
    public class RegisterOutput
    {
        public bool CanLogin { get; set; }
    }
}
