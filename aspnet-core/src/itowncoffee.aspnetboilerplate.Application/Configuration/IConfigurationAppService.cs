﻿using System.Threading.Tasks;
using itowncoffee.aspnetboilerplate.Configuration.Dto;

namespace itowncoffee.aspnetboilerplate.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
