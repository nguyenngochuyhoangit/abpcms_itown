﻿using Abp.Application.Services;
using itowncoffee.aspnetboilerplate.MultiTenancy.Dto;

namespace itowncoffee.aspnetboilerplate.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

