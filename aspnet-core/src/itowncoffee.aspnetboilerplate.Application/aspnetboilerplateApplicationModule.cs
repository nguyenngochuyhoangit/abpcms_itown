﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using itowncoffee.aspnetboilerplate.Authorization;

namespace itowncoffee.aspnetboilerplate
{
    [DependsOn(
        typeof(aspnetboilerplateCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class aspnetboilerplateApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<aspnetboilerplateAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(aspnetboilerplateApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
