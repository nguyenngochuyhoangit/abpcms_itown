﻿using System.Threading.Tasks;
using Abp.Application.Services;
using itowncoffee.aspnetboilerplate.Sessions.Dto;

namespace itowncoffee.aspnetboilerplate.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
