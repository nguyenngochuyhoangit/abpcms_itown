using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace itowncoffee.aspnetboilerplate.Controllers
{
    public abstract class aspnetboilerplateControllerBase: AbpController
    {
        protected aspnetboilerplateControllerBase()
        {
            LocalizationSourceName = aspnetboilerplateConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
