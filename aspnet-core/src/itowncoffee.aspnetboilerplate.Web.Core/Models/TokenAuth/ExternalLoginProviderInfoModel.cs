﻿using Abp.AutoMapper;
using itowncoffee.aspnetboilerplate.Authentication.External;

namespace itowncoffee.aspnetboilerplate.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
