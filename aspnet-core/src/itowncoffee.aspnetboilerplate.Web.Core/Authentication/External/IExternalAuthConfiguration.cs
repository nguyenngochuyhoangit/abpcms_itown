﻿using System.Collections.Generic;

namespace itowncoffee.aspnetboilerplate.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
