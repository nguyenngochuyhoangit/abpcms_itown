﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Abp.Authorization;
using itowncoffee.aspnetboilerplate.Authorization.Roles;
using itowncoffee.aspnetboilerplate.Authorization.Users;
using itowncoffee.aspnetboilerplate.MultiTenancy;
using Microsoft.Extensions.Logging;

namespace itowncoffee.aspnetboilerplate.Identity
{
    public class SecurityStampValidator : AbpSecurityStampValidator<Tenant, Role, User>
    {
        public SecurityStampValidator(
            IOptions<SecurityStampValidatorOptions> options,
            SignInManager signInManager,
            ISystemClock systemClock,
            ILoggerFactory loggerFactory) 
            : base(options, signInManager, systemClock, loggerFactory)
        {
        }
    }
}
