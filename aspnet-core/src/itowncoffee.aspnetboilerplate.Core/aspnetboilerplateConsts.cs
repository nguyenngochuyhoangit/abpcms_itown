﻿namespace itowncoffee.aspnetboilerplate
{
    public class aspnetboilerplateConsts
    {
        public const string LocalizationSourceName = "aspnetboilerplate";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
