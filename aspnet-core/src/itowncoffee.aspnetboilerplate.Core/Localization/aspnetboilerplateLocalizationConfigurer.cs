﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace itowncoffee.aspnetboilerplate.Localization
{
    public static class aspnetboilerplateLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(aspnetboilerplateConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(aspnetboilerplateLocalizationConfigurer).GetAssembly(),
                        "itowncoffee.aspnetboilerplate.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
