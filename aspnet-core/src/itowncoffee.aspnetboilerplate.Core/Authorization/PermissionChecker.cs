﻿using Abp.Authorization;
using itowncoffee.aspnetboilerplate.Authorization.Roles;
using itowncoffee.aspnetboilerplate.Authorization.Users;

namespace itowncoffee.aspnetboilerplate.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
