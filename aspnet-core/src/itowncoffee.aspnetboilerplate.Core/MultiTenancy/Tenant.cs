﻿using Abp.MultiTenancy;
using itowncoffee.aspnetboilerplate.Authorization.Users;

namespace itowncoffee.aspnetboilerplate.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
