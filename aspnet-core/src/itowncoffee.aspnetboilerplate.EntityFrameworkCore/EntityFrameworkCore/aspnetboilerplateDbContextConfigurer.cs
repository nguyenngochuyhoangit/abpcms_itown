using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace itowncoffee.aspnetboilerplate.EntityFrameworkCore
{
    public static class aspnetboilerplateDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<aspnetboilerplateDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<aspnetboilerplateDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
