﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using itowncoffee.aspnetboilerplate.Configuration;
using itowncoffee.aspnetboilerplate.Web;

namespace itowncoffee.aspnetboilerplate.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class aspnetboilerplateDbContextFactory : IDesignTimeDbContextFactory<aspnetboilerplateDbContext>
    {
        public aspnetboilerplateDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<aspnetboilerplateDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            aspnetboilerplateDbContextConfigurer.Configure(builder, configuration.GetConnectionString(aspnetboilerplateConsts.ConnectionStringName));

            return new aspnetboilerplateDbContext(builder.Options);
        }
    }
}
