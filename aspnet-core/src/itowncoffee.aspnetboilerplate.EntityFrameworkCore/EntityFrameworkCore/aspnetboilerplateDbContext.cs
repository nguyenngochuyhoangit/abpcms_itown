﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using itowncoffee.aspnetboilerplate.Authorization.Roles;
using itowncoffee.aspnetboilerplate.Authorization.Users;
using itowncoffee.aspnetboilerplate.MultiTenancy;

namespace itowncoffee.aspnetboilerplate.EntityFrameworkCore
{
    public class aspnetboilerplateDbContext : AbpZeroDbContext<Tenant, Role, User, aspnetboilerplateDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public aspnetboilerplateDbContext(DbContextOptions<aspnetboilerplateDbContext> options)
            : base(options)
        {
        }
    }
}
