﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using itowncoffee.aspnetboilerplate.Configuration;

namespace itowncoffee.aspnetboilerplate.Web.Host.Startup
{
    [DependsOn(
       typeof(aspnetboilerplateWebCoreModule))]
    public class aspnetboilerplateWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public aspnetboilerplateWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(aspnetboilerplateWebHostModule).GetAssembly());
        }
    }
}
