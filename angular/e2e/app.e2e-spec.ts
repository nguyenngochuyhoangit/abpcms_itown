import { aspnetboilerplateTemplatePage } from './app.po';

describe('aspnetboilerplate App', function() {
  let page: aspnetboilerplateTemplatePage;

  beforeEach(() => {
    page = new aspnetboilerplateTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
